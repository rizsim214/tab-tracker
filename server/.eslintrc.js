module.exports = {

    "globals": { 
        "angular": false,
         "module": false, 
         "inject": false,
         "document": false 
    },
    "env": {
        "browser": true, 
        "amd": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential"
    ],
    "parserOptions": {
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
    }
};
